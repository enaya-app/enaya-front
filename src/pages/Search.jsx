import React, { Fragment, useEffect, useState } from "react";
import DoctorList from "../components/DoctorList";
import Footer from "../components/Footer";
import Header from "../components/Header";
import Sidebar from "../components/Sidebar";
import MobileMenu from "../components/MobileMenu";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router";
import { SearchHandler } from "../store/actions/search";
import searchImage from "../assets/img/icons/search-tool.png";
import { useTranslation } from "react-i18next";

const Search = () => {
  const { searchData } = useSelector((state) => state.search);
  const dispatch = useDispatch();
  const [search, setSearch] = useState("");
  const [filters, setfilters] = useState({
    gender: undefined,
    type: undefined,
  });
  const { query } = useParams();
  const onSubmitHandler = (e = {}) => {
    e.hasOwnProperty("preventDefault") && e.preventDefault();
    console.log(search, filters.type, undefined, filters.gender);
    if (search.length) {
      dispatch(SearchHandler(search, filters.type, undefined, filters.gender));
    }
  };
  const { t } = useTranslation();
  useEffect(() => {
    if (query) {
      setSearch(query);
      dispatch(SearchHandler(query));
    }
  }, [query]);

  useEffect(() => {
    onSubmitHandler();
  }, [filters.gender, filters.type]);
  return (
    <Fragment>
      <Header />

      <section className="spec-detalis">
        <div className="container">
          <form
            id="form-search"
            onSubmit={onSubmitHandler}
            style={{ marginBottom: 20, marginTop: 20 }}
          >
            <input
              type="text"
              name="search"
              placeholder="Enter doctor name, category, specialist…"
              value={search}
              style={{ backgroundColor: "#f1efef" }}
              onChange={(e) => {
                const text = e.target.value;
                setSearch(text);
              }}
            />
            <img src={searchImage} alt="icon" />
            <button type="submit">{t("Find doctor")}</button>
          </form>
          <form>
            <div className="row">
              <div class="form-group" style={{ padding: 5 }}>
                <select
                  class="form-control"
                  id="exampleFormControlSelect1"
                  onChange={(e) => {
                    console.log(e.target.value);
                    const val = e.target.value;
                    setfilters((old) => ({ ...old, gender: val }));
                  }}
                >
                  <option disabled selected>
                    {t("Gender")}
                  </option>
                  <option value="m">{t("Male")}</option>
                  <option value="f">{t("Female")}</option>
                </select>
              </div>
              <div class="form-group" style={{ padding: 5 }}>
                <select
                  class="form-control"
                  id="exampleFormControlSelect1"
                  onChange={(e) => {
                    const val = e.target.value;
                    setfilters((old) => ({ ...old, type: val }));
                  }}
                >
                  <option disabled selected>
                    {t("Type")}
                  </option>
                  <option value="clinic">{t("Clinics")}</option>
                  <option value="hospital">{t("Hospitals")}</option>
                  {/* <option value="pharmacy">{t("Pharmacy")}</option>
                  <option value="company">{t("Company")}</option> */}
                </select>
              </div>
            </div>
          </form>

          <div className="row">
            <div className="col-lg-9">
              {searchData.map((doctor) => (
                <DoctorList
                  key={doctor.id}
                  Id={doctor.id}
                  name={doctor.name}
                  img={doctor.image}
                  specialist={doctor.departments}
                  location={doctor.address}
                  price={doctor.price}
                  workDays={doctor.wdays}
                  workHours={doctor.whours}
                  wating={doctor.waiting}
                  rate={doctor.patients}
                  rateValue={doctor.rate}
                  type={doctor.type}
                />
              ))}
            </div>
            <div className="col-lg-3">
              <Sidebar />
            </div>
          </div>
        </div>
      </section>

      <Footer />
      <MobileMenu />
    </Fragment>
  );
};

export default Search;
