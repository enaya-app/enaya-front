import React, { Fragment } from "react";
import { useState } from "react";
import { useDispatch } from "react-redux";
import Footer from "../components/Footer";
import Header from "../components/Header";
import MobileMenu from "../components/MobileMenu";
import FormInput from "../components/UI/Input";
import { RestPasswordHandler } from "../store/actions/auth";
import FormHeader from "../templates/Contact/FormHeader";

const ForgetPass = () => {
  const [email, setemail] = useState("");
  const dispatch = useDispatch();
  return (
    <Fragment>
      <Header />
      <section className="forget-pass-page text-center">
        <div className="container">
          <FormHeader
            title="Forgot your password ?"
            text="3 easy steps to reset your password, and enjoy your enaya account "
          />
          <form
            id="forget-form"
            onSubmit={(e) => {
              e.preventDefault();
              dispatch(RestPasswordHandler(email));
            }}
          >
            <FormInput
              tag={"input"}
              type={"email"}
              name={"phone"}
              onChange={(value) => {
                setemail(value);
              }}
              placeholder={"Enter Your Email"}
            />
            <FormInput
              tag={"input"}
              type={"submit"}
              name={"submit"}
              val={"Send Login Link"}
            />
          </form>
        </div>
      </section>
      <Footer />
      <MobileMenu />
    </Fragment>
  );
};

export default ForgetPass;
