import React, { Fragment, useState } from "react";
import { useTranslation } from "react-i18next";
import { useDispatch } from "react-redux";
import { ContactHandler } from "../../store/actions/auth";

const ContactForm = () => {
  const { t } = useTranslation();
  const [state, setstate] = useState({
    phone: "",
    name: "",
    message: "",
  });
  const dispatch = useDispatch();
  const onSubmitHandler = (e) => {
    e.preventDefault();
    dispatch(
      ContactHandler(state, () => {
        setstate(() => ({
          phone: "",
          name: "",
          message: "",
        }));
      })
    );
  };
  return (
    <Fragment>
      <form id="contactForm" onSubmit={onSubmitHandler}>
        <div className="single-input-item">
          <div className="row">
            <div className="col-md-6">
              <input
                type="text"
                name="name"
                placeholder={t("Enter Your Name *")}
                onChange={(e) => {
                  let value = e.target.value;
                  setstate((old) => ({ ...old, name: value }));
                }}
                required
              />
            </div>
            <div className="col-md-6">
              <input
                type="phone"
                name="phone"
                placeholder={t("Enter Your Phone *")}
                onChange={(e) => {
                  let value = e.target.value;
                  setstate((old) => ({ ...old, phone: value }));
                }}
                required
              />
            </div>

            <div className="col-12">
              <textarea
                name="message"
                cols="30"
                rows="7"
                placeholder={t("Your Meassge Here *")}
                onChange={(e) => {
                  let value = e.target.value;
                  setstate((old) => ({ ...old, message: value }));
                }}
                required
              ></textarea>
            </div>
            <div className="col-12">
              <input
                type="submit"
                name="submit"
                value={t("Send")}
                className="con-submit"
              />
            </div>
          </div>
        </div>
      </form>
    </Fragment>
  );
};

export default ContactForm;
