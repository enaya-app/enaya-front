import React, { useState } from "react";
import Cib from "../../assets/img/cib.png";
import Fawry from "../../assets/img/fawry.png";
import { Link } from "react-router-dom";
import { withTranslation } from "react-i18next";

import { useTranslation } from "react-i18next";
import { useSelector } from "react-redux";
import { axiosAPI, baseURL } from "../../helpers/config";

const BookForm = ({ date, time, id }) => {
  const { userData } = useSelector((state) => state.auth);
  const [state, setstate] = useState({
    name: userData.name,
    phone: userData.phone,
    email: userData.email,
    paymentType: null,
  });
  const { t } = useTranslation();
  const paymentHandler = async (e) => {
    e.preventDefault();
    if (state.paymentType !== null) {
      try {
        console.log({
          date,
          time,
          entity_id: id,
          patient_id: userData.id,
          payment_method: state.paymentType,
        });
        const { data } = await axiosAPI.post("appointments", {
          date,
          time,
          entity_id: id,
          patient_id: userData.id,
          payment_method: state.paymentType,
        });
        if (state.paymentType === "visa") {
          window.location = `${baseURL}payments/newWebView?payable_type=appointment&payable_id=${data.data.id}`;
        } else {
          window.location = "/complete-book";
        }
        console.log(data);
      } catch (error) {
        console.log(
          "🚀 ~ file: BookForm.js ~ line 25 ~ paymentHandler ~ error",
          error.response
        );
      }
    }
  };
  return (
    <form id="bookForm">
      <div className="single-input">
        <input
          type="text"
          className="test"
          name="name"
          value={state.name}
          placeholder={t("Patient Name")}
        />
        <input
          type="phone"
          className="test"
          name="phone"
          value={state.phone}
          placeholder={t("Phone Number")}
        />
        <input
          type="email"
          className="test"
          name="mail"
          value={state.email}
          placeholder={t("Email")}
        />
        {/* <label htmlFor="other" className="checklabel">Book for other</label> */}
        {/* <input type="checkbox" id="other" onChange={this.handelChecked} /> */}
        <div className="payment">
          <div className="payment-method">
            <h5>Payment</h5>
            <div className="row">
              <div className="col-4 cus">
                <input
                  type="radio"
                  id="cash"
                  className="cus-radio"
                  name="payment"
                  value="cash"
                  onChange={(input) => {
                    const value = input.target.value;
                    setstate((old) => ({ ...old, paymentType: value }));
                  }}
                />
                <label htmlFor="cash">{t("Cash")}</label>
              </div>
              <div className="col-4 cus">
                <input
                  type="radio"
                  id="cib"
                  className="cus-radio"
                  name="payment"
                  value="visa"
                  onChange={(input) => {
                    const value = input.target.value;
                    setstate((old) => ({ ...old, paymentType: value }));
                  }}
                />
                <label htmlFor="cib">
                  <img src={Cib} className="payment-img" alt="cib" />
                </label>
              </div>
              <div className="col-4 cus">
                <input
                  type="radio"
                  id="fawry"
                  className="cus-radio"
                  name="payment"
                  value=""
                />
                <label htmlFor="fawry">
                  <img src={Fawry} className="payment-img" alt="cib" />
                </label>
              </div>
            </div>
          </div>
          {/* <div className="row">
          <div className="col-12">
            <input type="text" name="card-num" placeholder="Card Number" />
          </div>
          <div className="col-8">
            <input type="text" name="card-num" placeholder="MM/YY" />
          </div>
          <div className="col-4">
            <input type="text" name="cvc" placeholder="CVC" />
          </div>
        </div> */}
        </div>
        {/* <input type="submit" name="book" value="book" className="con-submit" /> */}
        <Link className="costum-book" onClick={paymentHandler}>
          {t("Book")}
        </Link>
      </div>
    </form>
  );
};

export default BookForm;
