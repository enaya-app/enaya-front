import React, { useEffect, useState } from "react";
import {
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Row,
  Col,
} from "reactstrap";
import classnames from "classnames";
import FormInput from "../UI/Input";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { axiosAPI } from "../../helpers/config";

const DoctorBook = ({ name, img, price, specialist, id }) => {
  const [activeTab, setActiveTab] = useState(0);
  const [dates, setDates] = useState([]);
  const [dateTime, setDateTime] = useState({
    date: "",
    time: "",
  });
  const { t } = useTranslation();
  const toggle = (tab) => {
    if (activeTab !== tab) setActiveTab(tab);
  };
  const GetTimeHandler = async () => {
    if (dates.length === 0) {
      try {
        const { data } = await axiosAPI.get(
          `availabilities?filter[author_id]=${id}&filter[author_type]=entity`
        );
        const items = data.data[0].times;
        for (const key in items) {
          if (items[key][0] !== undefined)
            setDates((old) => [...old, items[key][0]]);
        }
      } catch (error) {
        console.log(error);
      }
    }
  };
  useEffect(() => {
    GetTimeHandler();
  }, []);
  useEffect(() => {
    setDateTime((old) => ({ ...old, date: dates[0] }));
  }, [dates]);

  function handelClick(e) {
    localStorage.setItem("DocName", name);
    localStorage.setItem("Docimg", img);
    localStorage.setItem("DocPrice", price);
    specialist.map((item) => localStorage.setItem("Spec", item.name));
  }

  return (
    <div className="booking-section">
      <h5 className="title">{t("Booking")} </h5>
      <div className="row equal">
        <div className="col-md-3">
          <h6 className="tab-title">{t("Available Dates")}</h6>
          <Nav tabs>
            {dates.map((item, index) => (
              <NavItem>
                <NavLink
                  className={classnames({ active: activeTab === index })}
                  onClick={() => {
                    toggle(index);
                    setDateTime((old) => ({ ...old, date: item }));
                  }}
                >
                  {item}
                </NavLink>
              </NavItem>
            ))}
          </Nav>
        </div>

        <div className="col-md-9">
          <h6 className="tab-title">Time</h6>
          <form>
            <TabContent activeTab={"1"}>
              <TabPane tabId="1">
                <Row>
                  <Col sm="12" className="pane-box">
                    <FormInput
                      tag={"input"}
                      ID={"test1"}
                      type={"radio"}
                      name={"date"}
                      onChange={() => {
                        setDateTime((old) => ({ ...old, time: "06:00" }));
                      }}
                      labelText={"6:00 PM"}
                    />
                    <FormInput
                      tag={"input"}
                      ID={"test2"}
                      type={"radio"}
                      name={"date"}
                      onChange={() => {
                        setDateTime((old) => ({ ...old, time: "07:00" }));
                      }}
                      labelText={"07:00 PM"}
                    />
                    <FormInput
                      tag={"input"}
                      ID={"test3"}
                      type={"radio"}
                      name={"date"}
                      onChange={() => {
                        setDateTime((old) => ({ ...old, time: "08:00" }));
                      }}
                      labelText={"08:00 PM"}
                    />
                    <FormInput
                      tag={"input"}
                      ID={"test4"}
                      type={"radio"}
                      name={"date"}
                      onChange={() => {
                        setDateTime((old) => ({ ...old, time: "09:00" }));
                      }}
                      labelText={"09:00 PM"}
                    />
                    <FormInput
                      tag={"input"}
                      ID={"test5"}
                      type={"radio"}
                      name={"date"}
                      onChange={() => {
                        setDateTime((old) => ({ ...old, time: "10:00" }));
                      }}
                      labelText={"10:00 PM"}
                    />
                  </Col>
                </Row>
              </TabPane>
              <TabPane tabId="2">
                <Row>
                  <Col sm="12" className="pane-box">
                    <FormInput
                      tag={"input"}
                      ID={"test1"}
                      type={"radio"}
                      name={"date"}
                      labelText={"12:00 PM"}
                    />
                    <FormInput
                      tag={"input"}
                      ID={"test2"}
                      type={"radio"}
                      name={"date"}
                      labelText={"12:00 PM"}
                    />
                    <FormInput
                      tag={"input"}
                      ID={"test3"}
                      type={"radio"}
                      name={"date"}
                      labelText={"12:00 PM"}
                    />
                    <FormInput
                      tag={"input"}
                      ID={"test4"}
                      type={"radio"}
                      name={"date"}
                      labelText={"12:00 PM"}
                    />
                    <FormInput
                      tag={"input"}
                      ID={"test5"}
                      type={"radio"}
                      name={"date"}
                      labelText={"12:00 PM"}
                    />
                    <FormInput
                      tag={"input"}
                      ID={"test6"}
                      type={"radio"}
                      name={"date"}
                      labelText={"12:00 PM"}
                    />
                  </Col>
                </Row>
              </TabPane>
              <TabPane tabId="3">
                <Row>
                  <Col sm="12" className="pane-box">
                    <FormInput
                      tag={"input"}
                      ID={"test1"}
                      type={"radio"}
                      name={"date"}
                      labelText={"12:00 PM"}
                    />
                    <FormInput
                      tag={"input"}
                      ID={"test2"}
                      type={"radio"}
                      name={"date"}
                      labelText={"12:00 PM"}
                    />
                    <FormInput
                      tag={"input"}
                      ID={"test3"}
                      type={"radio"}
                      name={"date"}
                      labelText={"12:00 PM"}
                    />
                    <FormInput
                      tag={"input"}
                      ID={"test4"}
                      type={"radio"}
                      name={"date"}
                      labelText={"12:00 PM"}
                    />
                  </Col>
                </Row>
              </TabPane>
              <TabPane tabId="4">
                <Row>
                  <Col sm="12" className="pane-box">
                    <FormInput
                      tag={"input"}
                      ID={"test1"}
                      type={"radio"}
                      name={"date"}
                      labelText={"12:00 PM"}
                    />
                  </Col>
                </Row>
              </TabPane>
              {dateTime.time.length !== 0 && dateTime.date.length !== 0 ? (
                <Link
                  to={`${
                    process.env.PUBLIC_URL +
                    `/book/${id}/${dateTime.date}/${dateTime.time}`
                  }`}
                  onClick={handelClick}
                  className="costum-book"
                >
                  {t("Book")}
                </Link>
              ) : (
                ""
              )}
            </TabContent>
          </form>
        </div>
      </div>
    </div>
  );
};

export default DoctorBook;
