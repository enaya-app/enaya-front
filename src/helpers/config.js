import axios from "axios";
export const LocalKeys = {
  TOKEN: "TOKEN",
  LANG: "LANG",
};
export const baseURL = "https://enaya-app.enaya.life/api/v1";
export const axiosAPI = axios.create({
  baseURL: baseURL,
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
    "Accept-Language": localStorage.getItem(LocalKeys.LANG) || "en",
    Authorization: `Bearer ${localStorage.getItem(LocalKeys.TOKEN)}`,
  },
});
